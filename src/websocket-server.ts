import * as Promise from 'bluebird';
import * as EventEmitter from 'eventemitter3';
import { Server } from 'uws';
import Socket = NodeJS.Socket;

export type IWebSocketServerAuth = (info: object, callback: (res: boolean) => void) => void;

export class WebSocketServer extends EventEmitter {
  private readonly port: number;
  private server?: Server;

  public constructor(port: number) {
    super();
    this.port = port;
  }

  public start(): Promise<void> {
    return new Promise(
      (resolve: () => void, reject: (err: Error) => void): void => {
        const verifyClient: IWebSocketServerAuth = (info, callback) => this.emit('auth', info, callback);

        const server = new Server(
          {
            port: this.port,
            verifyClient,
          },
          (): void => {
            server.removeListener('error', reject);
            resolve();
            return;
          }
        );

        server.once('error', reject);
        server.on('connection', (client: Socket) => this.emit('connection', client));
        this.server = server;
      }
    );
  }

  public stop(): Promise<void> {
    return new Promise(
      (resolve: () => void, reject: (err: Error) => void): void => {
        const server = this.server;

        if (server === undefined) {
          resolve();
          return;
        }

        try {
          server.removeAllListeners();
          server.close();
        } catch (err) {
          reject(err);
          return;
        }

        this.server = undefined;
        resolve();
      }
    );
  }
}

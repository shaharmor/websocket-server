import * as WebSocket from 'ws';
import { IWebSocketServerAuth, WebSocketServer } from './websocket-server';

describe('websocket-server', () => {
  const port: number = 8888;
  const approve: IWebSocketServerAuth = (info, callback) => callback(true);
  let server: WebSocketServer;
  let client: WebSocket;

  beforeEach(() => {
    server = new WebSocketServer(port);
  });

  afterEach(async () => {
    await server.stop();
    await client.close();
  });

  describe('.start()', () => {
    test('starts a websocket server', async (done) => {
      server.on('auth', approve);
      await server.start();
      client = new WebSocket(`ws://localhost:${port}`);
      client.on('open', done);
    });

    test('fails to start if port is taken', async () => {
      server.on('auth', approve);
      await server.start();
      const server2 = new WebSocketServer(port);
      server.on('auth', approve);
      try {
        await server2.start();
        throw new Error('server started while port is taken');
      } catch (err) {
        expect(err.code).toEqual('EADDRINUSE');
      }
    });
  });

  describe('.stop()', () => {
    test('stops the websocket server', async (done) => {
      server.on('auth', approve);
      await server.start();
      client = new WebSocket(`ws://localhost:${port}`);
      client.on('open', async () => {
        await server.stop();
        const client2: WebSocket = new WebSocket(`ws://localhost:${port}`);
        client2.on('open', () => {
          done(new Error('server still working after being stopped'));
        });
        client2.on('error', (err: any) => {
          try {
            expect(err.code).toEqual('ECONNREFUSED');
            done();
          } catch (err2) {
            done(err2);
          }
        });
      });
    });

    test('closes all active connections', async (done) => {
      server.on('auth', approve);
      await server.start();
      client = new WebSocket(`ws://localhost:${port}`);
      client.on('open', async () => {
        client.on('close', () => {
          done();
        });
        await server.stop();
      });
    });
  });

  describe('authentication', () => {
    test('aborts user connection on failed auth', async (done) => {
      server.on('auth', (info, callback) => callback(false, 401));
      await server.start();
      client = new WebSocket(`ws://localhost:${port}`);
      client.on('open', () => {
        done(new Error('client connection should not open'));
      });
      client.on('error', (err: Error) => {
        expect(err.message).toEqual('Unexpected server response: 401');
        done();
      });
    });

    test('can abort using different status code', async (done) => {
      server.on('auth', (info, callback) => callback(false, 403));
      await server.start();
      client = new WebSocket(`ws://localhost:${port}`);
      client.on('open', () => {
        done(new Error('client connection should not open'));
      });
      client.on('error', (err) => {
        expect(err.message).toEqual('Unexpected server response: 403');
        done();
      });
    });

    test('accepts user connection on valid auth', async (done) => {
      server = new WebSocketServer(port);
      server.on('auth', (info, callback) => callback(true));
      await server.start();
      client = new WebSocket(`ws://localhost:${port}`);
      client.on('open', done);
    });
  });
});
